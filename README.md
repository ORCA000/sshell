# SShell: A end to end encrypted reverse shell with a custom key exchange algorithm


#### How Does it Work? 
* first, you'll have to run the server, that will listen for incoming connections, but it won't verify any new clients, instead, we [assume](https://gitlab.com/ORCA666/sshell/-/blob/main/Server/Server.h#L123) that the first socket from an original client will be a 6-byte hash carrying the same hash as in our server. this hash, is updated every 2 minutes, on both sides (server and client), that will be later used as a key for [Rc4](https://gitlab.com/ORCA666/sshell/-/blob/main/Server/functions.h#L127) and [XOR](https://gitlab.com/ORCA666/sshell/-/blob/main/Server/functions.h#L36) encryption/decryption.
* now we have our connection verified, the server will ask for commands, at input, we encrypt the command using Rc4 and send it. 
* the client then decodes the received command, create cmd process (only once entire the whole time), read the output using [pipes](https://gitlab.com/ORCA666/sshell/-/blob/main/S-Shell/S-Shell.h#L273), send the output's buffer size to the server to do the proper [allocation](https://gitlab.com/ORCA666/sshell/-/blob/main/Server/functions.h#L75) to reserve the next socket's output.
* after that, it encrypts the output with [XOR](https://gitlab.com/ORCA666/sshell/-/blob/main/S-Shell/S-Shell.h#L336) function, and send the buffer to the server, that will decode and display.

#### Advantages:
* spawns only one cmd process, during the whole execution time.
* commands are rc4 encrypted
* outputs are xor encrypted
* a key exchange algorithm to verify clients
* the key used in both encryptions (rc4 and xor) changes every 2 minutes, based on google authenticator.

#### This repo is published not because it is a complete module, but because i wanted to share a better idea for doing c2 communication, and i just wanted to see what i can do with sockets. hoping someone will use it in a real scenario.

#### DEMO:
![image1](https://gitlab.com/ORCA666/sshell/-/raw/main/images/netsh_wlan_show_all.png)
![image2](https://gitlab.com/ORCA666/sshell/-/raw/main/images/ping.png)



# THANKS FOR:
* [@t57root](https://github.com/t57root) for [amcsh](https://github.com/t57root/amcsh)
* [@d4rk007](https://github.com/d4rk007) for [sak1to-shell](https://github.com/d4rk007/sak1to-shell)
* [redirect input output piped from parent process to cmd child process c/c++](https://stackoverflow.com/questions/62654569/redirect-input-output-pipes-from-parent-process-to-cmd-child-process-c-c-winap)


##### [NOTE]: although the project may be simple, it is somehow not, and requires a lot of extra care under a debugger to check what is happening. so far, i had couple of bugs still, i wanted to be as dynamic as possible and read all outputs (not size limited) and i think that's the cause of the bugs. Anyways, the project is still buggy, and i don't have any intention to fix it, because I've been stuck in a debugger for a while now, and I really love to do something else, in case u want to help, or have a better version idea, I will be happy looking at it. 


<h6 align="center"> <i>#                                   STAY TUNED FOR MORE</i>  </h6> 
![120064592-a5c83480-c075-11eb-89c1-78732ecaf8d3](https://gitlab.com/ORCA666/kcthijack/-/raw/main/images/PP.png)





