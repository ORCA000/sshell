#pragma once

#include "functions.h"
#include "otp.h"


#define IP "127.0.0.1"
#define PORT "8080"


#define TIME_BETWEEN_UPDATE 120000
#define TIME_OUT 10 // sec

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// update our hash in the struct
void HashUpdate() {
	while (TRUE) {
		strcpy(Data.OlderHash, (char*)Data.Hash);
		get_otp((char*)Data.Hash);
		//printf("[i] Data.OlderHash : %s \n", Data.OlderHash);
		//printf("[i] Data.Hash : %s \n", Data.Hash);
        Sleep(TIME_BETWEEN_UPDATE);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// compare hashes
BOOL CompareHashes(char* Hash) {
    //printf("[i] [CompareHashes] Comapring : [In Hash] %s to %s [Current Hash] || %s [Older Hash] \n", Hash, Data.Hash, Data.OlderHash);
    if (atoi(Hash) == atoi(Data.Hash) || atoi(Hash) == atoi(Data.OlderHash)) {
        //printf("[+] We Have A Match ! : %s \n", Hash);
        printf("[+] VERIFIED \n");
        return TRUE;
    }
    printf("[-] NOT VERIFIED \n");
    return FALSE;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// create a socket
SOCKET CreateSocket() {
    SOCKET server;
    WSADATA wsa;
    int n = 1;
    struct sockaddr_in Server = { 0 };
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
        printf("[!] WSAStartup failed with error code : %d \n", WSAGetLastError());
        return INVALID_SOCKET;
    }

    if ((server = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) {
        printf("[!] socket failed with error code : %d", WSAGetLastError());
        return INVALID_SOCKET;
    }

    // https://docs.microsoft.com/en-us/windows/win32/api/winsock/nf-winsock-setsockopt
    if (setsockopt(server, SOL_SOCKET, SO_REUSEADDR, (void*)&n, sizeof(n)) == INVALID_SOCKET) {
        printf("[!] setsockopt failed with error code : %d", WSAGetLastError());
        return INVALID_SOCKET;
    }

    Server.sin_family = AF_INET;
    Server.sin_port = htons(atoi(PORT));
    Server.sin_addr.s_addr = INADDR_ANY;


    if (bind(server, (struct sockaddr*)&Server, sizeof(Server)) == SOCKET_ERROR) {
        printf("[!] bind failed with error code : %d", WSAGetLastError());
        return INVALID_SOCKET;
    }


    if (listen(server, 5) < 0) {
        printf("[!] listen failed with error code : %d", WSAGetLastError());
        return INVALID_SOCKET;
    }

    return server;
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// recieve hash and verify
int HandShake(SOCKET client) {
    int ret;
    char buffer[8];
    printf("[i] Verifying Client ... ");
    if((ret = recv(client, buffer, strlen(Data.Hash), 0)) <= 0) {
        printf("[!] recv Error %d", WSAGetLastError());
        return FAILURE;
    }
    if (buffer != NULL){
        buffer[strlen(buffer) + 1] = '\00';
        //printf("[+] Recieved Buffer: %s of Size : %d \n", buffer, strlen(buffer));
        if (CompareHashes(buffer)) {
            return SUCCESS;
        }
        else {
            return FAILURE;
        }
    }
    return FAILURE;
}




int SListen(SOCKET server) {
    if (server == INVALID_SOCKET){
        printf("[!] Invalid Socket : %d", WSAGetLastError());
        return;
    }
    SOCKET client;
    struct sockaddr_in Client = { 0 };
    int n = sizeof(Client);
    printf("[+] Waiting For New Connections ... \n");
    while ((client = accept(server, (struct sockaddr*)&Client, (socklen_t * __restrict) & n)) != INVALID_SOCKET) {
        if ((Client.sin_port != NULL) && (&Client.sin_addr != NULL)) {
            char* client_ip = inet_ntoa(Client.sin_addr);
            int client_port = ntohs(Client.sin_port);
            printf("[+] Connection from  %s:%d \n", client_ip, client_port);
           
            if (HandShake(client) == SUCCESS) {
                Data.Verified = TRUE;
            }
            else{
                Data.Verified = FALSE;
                return NOTVARF;
            }
            if (Data.Verified == TRUE) {
                DWORD timeout = TIME_OUT * 1000;
                setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);
                SendCommands(client, client_ip);
                return SUCCESS;
            }
        }
        closesocket(client);
        WSACleanup();
    }
    printf("[!] accept failed with error code : %d", WSAGetLastError());
    return FAILURE;
}


