#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct rc4_state {
    int x, y, m[256];
};


void InitRc4(struct rc4_state* s, unsigned char* key, int length) {
    int i, j, k, * m, a;

    s->x = 0;
    s->y = 0;
    m = s->m;

    for (i = 0; i < 256; i++)
    {
        m[i] = i;
    }

    j = k = 0;

    for (i = 0; i < 256; i++)
    {
        a = m[i];
        j = (unsigned char)(j + a + key[k]);
        m[i] = m[j]; m[j] = a;
        if (++k >= length) k = 0;
    }
}

void RC4(struct rc4_state* s, unsigned char* data, int length) {
    int i, x, y, * m, a, b;

    x = s->x;
    y = s->y;
    m = s->m;

    for (i = 0; i < length; i++)
    {
        x = (unsigned char)(x + 1); a = m[x];
        y = (unsigned char)(y + a);
        m[x] = b = m[y];
        m[y] = a;
        data[i] ^= m[(unsigned char)(a + b)];
    }

    s->x = x;
    s->y = y;
}


void XOR(char* data, size_t data_len, char* key, size_t key_len) {
    int j;

    j = 0;
    for (int i = 0; i < data_len; i++) {
        if (j == key_len - 1) j = 0;

        data[i] = data[i] ^ atoi(key) ^ j;
        j++;
    }
}

/*
void XOR2(char* data, size_t data_len, char* key) {
    for (int i = 0; i < data_len + 1; i++) {
        data[i] ^ atoi(key);
    }
}
*/